//
//  SignUpViewController.swift
//  WorkdayHologram
//
//  Created by mac on 12/11/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import UIKit
import Alamofire


let validDomains = ["paracosma.com", "workday.com"]



class SignUpViewController: UIViewController {

    var registerCase = registerCases.register
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var emailTextField: PaddableTextField!
    
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var passwordTextField: PaddableTextField!
    @IBOutlet weak var topconstraint: NSLayoutConstraint!
    @IBOutlet weak var usernamestackview: UIStackView!
    @IBOutlet weak var usernameTextField: PaddableTextField!
    @IBOutlet weak var accountButton: UIButton!
    var alertView: AlertRibbonView!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        checkTitle()
        alertView = AlertRibbonView(frame: CGRect(x: 0, y: -70, width: self.view.frame.width, height: 70))
        self.view.addSubview(alertView)
        setHeaderLabel()
    }
    
    func checkTitle(){
        
        switch registerCase {
        case .forgotPassword:
            self.passwordStackView.isHidden = true
            usernamestackview.isHidden = true
            topconstraint.constant += (usernamestackview.frame.height-20)
            accountButton.setTitle("Reset Password", for: .normal)
        default:
            self.passwordStackView.isHidden = false
            usernamestackview.isHidden = false
            topconstraint.constant = 70
            accountButton.setTitle("Register", for: .normal)
        }
        
    }
    
    func setHeaderLabel(){
        headerLabel.text = registerCase.rawValue
    }
    
    override var shouldAutorotate: Bool {
          return false
      }

      override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
          return UIInterfaceOrientationMask.portrait
      }

      override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
          return UIInterfaceOrientation.portrait
      }
    
    @IBAction func signUpBtnTapped(_ sender: Any) {
        
        let email = emailTextField.text ?? ""
        let username = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        if !email.validate(validDomains: validDomains){
            self.alertView.showAlert(alertCase: alertCases.invalidEmail)
        }
        if email != ""{
            switch registerCase{
            case .forgotPassword:
                postForgotPassword(email: email)
                break
            default:
                if username != "" && password != ""{
                    postRegister(email: email,username:username, password: password)
                }else{
                    if username == ""{
                        self.alertView.showAlert(alertCase: alertCases.noUsername)
                    }else{
                        self.alertView.showAlert(alertCase: alertCases.noPassword )
                    }
                   
                }
                break
            }
        }else{
            self.alertView.showAlert(alertCase: alertCases.noEmail)
        }
        
    }
    
    func postForgotPassword(email: String){
        
        if !email.validate(validDomains: validDomains){
            self.alertView.showAlert(alertCase: alertCases.invalidEmail)
        }
        let url = "\(URLs.baseUrl)\(URLs.forgotPassword)"  // This will be your link
        let parameters: Parameters = ["email": email]    //This will be your parameter
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            print(response.result)
            if let json = response.result.value as? [String: Any]{
                
                self.alertView.showAlert(alertCase: alertCases.forgotPassword)
                if let code = json["code"] as? String{
                    print("sanam: ",code, json)
                    switch code{
                    case "0025":
                        self.alertView.showAlert(alertCase: alertCases.notRegistered)
                        break
                    case "0026":
                        self.alertView.showAlert(alertCase: alertCases.unverified)
                        break
                    default:
                        self.alertView.showAlert(alertCase: alertCases.forgotPassword)
                        break
                    }
                }
            }
        }
    }
    
 
    
    func postRegister(email: String,username:String,password:String){
        
        if email.validate(validDomains: validDomains){
        
            let url = "\(URLs.baseUrl)\(URLs.register)"  // This will be your link
            let parameters: Parameters = ["email": email,"name":username,"password":password]    //This will be your parameter
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            if let json = response.result.value as? [String: Any]{
                
                print("auxx ", json)
                if let code = json["code"] as? String{
                    
                   
                    switch code{
                    case "0004":
                        self.alertView.showAlert(alertCase: alertCases.alreadyRegistered)
                        break
                    default:
                        self.alertView.showAlert(alertCase: alertCases.signup)
                        break
                    }
                }
            }
        }
        }else{
            self.alertView.showAlert(alertCase: alertCases.invalidEmail)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}





extension String{
    
    func validate(validDomains:[String]) -> Bool {
        let REGEX: String
        if let domain = self.components(separatedBy: "@").last, validDomains.contains(domain) {
            // Entered email has valid domain.
            
            REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
            return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: self)
        }
       
        return false
    }
}

//
//  AlertRibbonView.swift
//  WorkdayHologram
//
//  Created by mac on 12/11/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import UIKit

class AlertRibbonView: UIView {

    var alertLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAlertView()
    }
    
    func setupAlertView(){
        self.backgroundColor = .red
        
        alertLabel = UILabel(frame: CGRect(x: 30, y: 16, width: self.frame.width - 60, height: 70))
        alertLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        alertLabel.text = "asd"
        alertLabel.numberOfLines = 0
        alertLabel.textColor = .white
        alertLabel.textAlignment = .center
        
        self.addSubview(alertLabel)
        
//        addConstraint()
    }
    
    func addConstraint(){
        
        alertLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([alertLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),alertLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),alertLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),alertLabel.heightAnchor.constraint(equalToConstant: 40)])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showAlert(alertCase: alertCases){
        alertLabel.text = alertCase.rawValue
    
        switch alertCase {
        case .signup, .forgotPassword:
            self.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        default:
            self.backgroundColor = .red
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        }) { (bool) in
            DispatchQueue.main.asyncAfter(deadline: .now()+3, execute: {
                self.hideAlert()
            })
        }
    }
    
    func hideAlert(){
        UIView.animate(withDuration: 0.2) {
            self.frame = CGRect(x: 0, y: -70, width: self.frame.width, height: self.frame.height)
        } completion: { (com) in
            
            
        }

        
    }
}

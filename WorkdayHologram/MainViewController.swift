//
//  MainViewController.swift
//  WorkdayHologram
//
//  Created by VRLab on 10/31/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import UIKit
import ARKit
import Firebase

class MainViewController: UIViewController {
    var videoSegmentsList = [Segments](){
        didSet {
            videoSegmentsList = videoSegmentsList.sorted {$0.id < $1.id }
            
            self.videoSegmentsTableView.reloadData()
        }
    }
    
    var n = false
    
    @IBOutlet weak var videoPlayer: CustomControls!
    var currentExecutive: Executives?
    var executiveList: [Executives]!
    var db = Database()
    
    @IBOutlet weak var dummyLabel: UILabel!
    
    @IBOutlet weak var executiveImageView: UIImageView!
    @IBOutlet weak var executiveNameLabel: UILabel!
    @IBOutlet weak var executivePositionLabel: UILabel!

    @IBOutlet weak var segmentsDetailStackView: UIStackView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var segmentsExecutiveNameLabel: UILabel!
    @IBOutlet weak var segmentsExecutivePositionLabel: UILabel!
    
    @IBOutlet weak var ArInstructionLabel: UILabel!
    @IBOutlet weak var domainLabel: UILabel!
    var c = 0
    var g = 1000
    var p = 1000
    
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var videoStackView: UIStackView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var videoPlayerView: UIView!
    @IBOutlet weak var videoSegmentsView: UIView!
    @IBOutlet weak var playerDisplayView: UIView!
    
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var videoSegmentsTableView: UITableView!
    @IBOutlet weak var playerControlView: UIView!
    
    @IBOutlet weak var playerTimerProgressConstraint: NSLayoutConstraint!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    
    
    var videoIsPlaying = false{
        didSet{
            if !videoIsPlaying{
//                self.stopVideo()
            }
        }
    }
    var showOval = true
    var watchAll = false
//    var firstWatch = true
    
    var tapGesture: UITapGestureRecognizer!
    
    var nextIndexPath: IndexPath!
    
    var plane1 = SCNNode()
    var plane2 = SCNNode()
    var ovalNode = SCNNode()
    var node = SCNNode()
    

    var center: CGPoint!
    
    var chromaKeyContent: VideoContent?
    
    var prevDomain = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.setUpSceneView()
//        self.setupNodes()
        self.executiveList = self.db.getExecutives().filter(){$0.id != 10}
        print(executiveList.count)
        
        let frame = view.frame
        center = CGPoint (x: frame.width/2, y: frame.height/2)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapListener(_:)))
        self.playerDisplayView.addGestureRecognizer(tapGesture)
        
        self.videoSegmentsTableView.rowHeight = UITableView.automaticDimension
        self.videoSegmentsTableView.estimatedRowHeight = 70
    }
    
    @IBAction func setup(_ sender: UIButton){
        videoIsPlaying = false
        showOval = true
        watchAll = false
//        stopVideo()
        ovalNode.removeFromParentNode()
//        setupNodes()
//        setUpSceneView()
        playerControlView.isHidden = true
        resetButton.isHidden = true
        
        ArInstructionLabel.isHidden = false
        ArInstructionLabel.text = "Please point the camera towards the floor and slowly scan for surface"
    }
    
    @objc func tapListener(_ recognizer: UITapGestureRecognizer){
        if videoIsPlaying && !showOval{
            playerControlView.isHidden = !playerControlView.isHidden
            resetButton.isHidden = !resetButton.isHidden 
        }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        if self.videoPlayerView.isHidden == true {
            self.videoPlayerView.isHidden = false
//            self.videoTitleLabel.text = self.currentExecutive?.story
            self.dummyLabel.isHidden = true
            self.segmentsDetailStackView.isHidden = true
        } else {
            self.collectionView.isHidden = false
            self.videoStackView.isHidden = true
            self.backBtn.isHidden = true
            self.footerView.isHidden = false
            if !showOval{
                self.videoIsPlaying = false
            }
            self.playerControlView.isHidden = true
            resetButton.isHidden = true
        }
    }
    
    @IBAction func watchAllBtnTapped(_ sender: Any) {
        if videoSegmentsList.count > 0{
            watchAll = true
            n = true
            nextIndexPath = IndexPath(row: 0, section: 0)
            videoSegmentsTableView.selectRow(at: nextIndexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
            videoSegmentsTableView.delegate?.tableView!(videoSegmentsTableView, didSelectRowAt: nextIndexPath)
        }
        
    }
    
    @IBAction func backwardBtnTapped(_ sender: Any) {
        if let ckc = chromaKeyContent{
            let currentTime = CMTimeGetSeconds(ckc.player.currentTime())
            var newTime = currentTime - 5.0
            
            if newTime < 0{
                newTime = 0
            }
            let time: CMTime = CMTimeMake(value: Int64(newTime*1000), timescale: 1000)
            ckc.player.seek(to: time)
        }
    }
    
    @IBAction func playPauseBtnTapped(_ sender: UIButton) {
        if let ckc = chromaKeyContent{
            if ckc.isPlaying{
                ckc.player.pause()
                sender.setImage(UIImage(named: "player-play"), for: .normal)
            }else{
                
                guard let duration = ckc.player.currentItem?.duration else { return }
                let currentTime = CMTimeGetSeconds(ckc.player.currentTime())
                if currentTime == CMTimeGetSeconds(duration){
                    ckc.player.seek(to: CMTime.zero)
                }
                
                ckc.player.play()
                sender.setImage(UIImage(named: "player-pause"), for: .normal)
            }
            ckc.isPlaying = !ckc.isPlaying
        }
    }
    
    @IBAction func forwardBtnTapped(_ sender: Any) {
        if let ckc = chromaKeyContent{
            guard let duration = ckc.player.currentItem?.duration else { return }
            let currentTime = CMTimeGetSeconds(ckc.player.currentTime())
            var newTime = currentTime + 5.0
            if newTime > (CMTimeGetSeconds(duration)){
                newTime = CMTimeGetSeconds(duration)
            }
            let time: CMTime = CMTimeMake(value: Int64(newTime*1000), timescale: 1000)
            ckc.player.seek(to: time)
        }
    }
    
    @IBAction func signOutBtnTapped(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: defaultsKeys.loggedIn)
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navController")
        UIApplication.shared.delegate?.window??.rootViewController = vc
    }
}


extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videoSegmentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoSegmentsTableViewCell", for: indexPath) as! VideoSegmentsTableViewCell
        let segments = self.videoSegmentsList[indexPath.row]
        cell.segmentsTitleLabel.text = segments.title
        cell.videoStartingTimeLabel.text = segments.startingTime
        
        let selectionView = UIView()
        selectionView.backgroundColor = UIColor(red: 29/255, green: 64/255, blue: 103/255, alpha: 0.5)
        cell.selectedBackgroundView =  selectionView
        
        return cell
    }

}
extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !n{
            watchAll = false
        }
        self.videoPlayerView.isHidden = true
        
        self.dummyLabel.isHidden = false
        self.segmentsDetailStackView.isHidden = false
        let segments = self.videoSegmentsList[indexPath.row]
            if indexPath.row + 1 == videoSegmentsList.count{
                self.nextIndexPath = IndexPath(row: 0, section: 0)

                self.watchAll = false
            }else{
                self.nextIndexPath = IndexPath(row: indexPath.row + 1, section: 0)
            }
        videoPlayer.videoPlayer.videolist(filename: "")
//        self.setupVideoContent(video: segments.video ?? "intro.mp4")
        n = false
    }
    
}

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.executiveList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExecutivesCollectionViewCell", for: indexPath) as! ExecutivesCollectionViewCell
        let executive = self.executiveList[indexPath.row]
        cell.setUpCell(executive: executive)
        cell.hideFlagView()
        
        return cell
    }
    
}

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.videoStackView.isHidden = false
        self.collectionView.isHidden = true
        self.backBtn.isHidden = false
        self.footerView.isHidden = true
        let executive = self.executiveList[indexPath.row]
        
     
        self.currentExecutive = executive
        self.videoSegmentsList = executive.segments?.allObjects as? [Segments] ?? []
        self.executiveImageView.image = UIImage(data: (executive.image ?? Data()))
        self.executiveNameLabel.text = executive.name
        self.executivePositionLabel.text = executive.title
        self.videoTitleLabel.text = "\"\(executive.story ?? " `")\""
        self.segmentsExecutiveNameLabel.text = executive.name
        self.segmentsExecutivePositionLabel.text = executive.title
        if !showOval{
            videoIsPlaying = true
        } else {
            ArInstructionLabel.isHidden = false
            if !videoIsPlaying{
                ArInstructionLabel.text = "Please point the camera towards the floor and slowly scan for surface"
            }
        }
        Analytics.logEvent("leader_tapped", parameters: ["event_category": "Leader Tapped",
                                                         "event_action": "Leader Tapped Action",
                                                         "event_name": self.executiveNameLabel.text ?? ""
                                                        ])
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cells = self.collectionView.visibleCells as! [ExecutivesCollectionViewCell]
        var c = 0
        var g = 0
        var p = 0
        for cell in cells{
            switch cell.executive.designation{
            case "Corporate Functions":
                c += 1
                break
                
            case "Go-to-Market":
                g += 1
                break
                
            case "Products & Technology":
                p += 1
                break
                
            default:
                break
            }
        }
        if c >= 3{
            UIView.animate(withDuration: 0.2, animations: {
                self.footerView.backgroundColor = UIColor(red: 15/255, green: 116/255, blue: 188/255, alpha: 1)
                 
            }){ (bool) in
                self.domainLabel.text = "Corporate Functions"
            }
            
            
        }else if g >= 3{
            UIView.animate(withDuration: 0.2, animations: {
                self.footerView.backgroundColor = UIColor(red: 64/255, green: 180/255, blue: 229/255, alpha: 1)
                
            }){ (bool) in
                self.domainLabel.text = "Go-to-Market"
            }
        }else if p >= 3{
            UIView.animate(withDuration: 0.2, animations: {
                self.footerView.backgroundColor = UIColor(red: 0/255, green: 109/255, blue: 182/255, alpha: 1)
                
            }){ (bool) in
                self.domainLabel.text = "Products & Technology"
            }
        }
        print("vvvvv: ", c,g,p)
    }
}


////
////  ARVideo+MainViewController.swift
////  WorkdayHologram
////
////  Created by mac on 11/5/18.
////  Copyright © 2018 VRLab. All rights reserved.
////
//
//import Foundation
//import ARKit
//
//extension MainViewController: ARSCNViewDelegate{
//    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
//        if anchor is ARPlaneAnchor{
//            
//            self.ovalNode = SCNNode(geometry: SCNPlane(width: 0.4, height: 0.4))
//            self.ovalNode.runAction(SCNAction.rotateBy(x: -.pi/2, y: 0, z: 0, duration: 0))
//            self.ovalNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "oval")
//            ovalNode.geometry?.firstMaterial?.isDoubleSided = true
//            node.addChildNode(self.ovalNode)
//            self.node = node
//            self.node.addChildNode(plane2)
//            self.node.addChildNode(plane1)
//            
//            let configuration = sceneView.session.configuration as! ARWorldTrackingConfiguration
//            configuration.planeDetection = .init(rawValue: 0)
//            sceneView.session.run(configuration)
//            
//            videoIsPlaying = true
//            DispatchQueue.main.async {
//                self.ArInstructionLabel.text = "Please move the iPad to position the circle in the floor where you want the hologram to appear"
//            }
//        }
//    }
//    
//    func setupVideoContent(video: String) {
//        if videoIsPlaying{
//            if showOval{
//                showOval = false
//                
//                ArInstructionLabel.isHidden = true
//                let yFreeConstraint = SCNBillboardConstraint()
//                yFreeConstraint.freeAxes = .Y // optionally
//                node.constraints = [yFreeConstraint] //
//                
//                ovalNode.removeFromParentNode()
//            }
//            stopVideo()
//            
////            if firstWatch{
//                playerControlView.isHidden = false
//                resetButton.isHidden = false
////                firstWatch = false
////            }
//            
//            print(nextIndexPath)
//            if video.lowercased().contains("intro") {
//                chromaKeyContent = VideoContent(filename: video, size: CGSize(width: 2160, height: 2160), sender: self)
//                let chromaKeyMaterial = ChromaKeyMaterial()
//                chromaKeyMaterial.diffuse.contents = chromaKeyContent
//                
//                
//                plane1.geometry!.materials = [chromaKeyMaterial]
//                node.addChildNode(plane1)
//            }else{
//                chromaKeyContent = VideoContent(filename: video, size: CGSize(width: 1080, height: 1660), sender: self)
//                let chromaKeyMaterial = ChromaKeyMaterial()
//                chromaKeyMaterial.diffuse.contents = chromaKeyContent
//                
//                
//                plane2.geometry!.materials = [chromaKeyMaterial]
//                node.addChildNode(plane2)
//            }
//            
////            let chromaKeyMaterial = ChromaKeyMaterial()
////            chromaKeyMaterial.diffuse.contents = chromaKeyContent
////            
////            
////            plane1.geometry!.materials = [chromaKeyMaterial]
////            node.addChildNode(plane1)
//            
////            plane1.runAction(SCNAction.moveBy(x: 0, y: -0.1, z: 0, duration: 0.01))
//            
//        }
//    }
//    
//    func stopVideo(){
//        if let c = chromaKeyContent{
//            c.player.pause()
//            
//            c.removeObserver()
//            c.player = AVPlayer()
////            NotificationCenter.default.removeObserver(c)
////            c.player.removeTimeObserver(c.timeObserver)
//        }
//        chromaKeyContent = nil
//        plane1.removeFromParentNode()
//        plane2.removeFromParentNode()
//        playPauseButton.setImage(UIImage(named: "player-play"), for: .normal)
//        currentTimeLabel.text = "00:00"
//        totalTimeLabel.text = "00:00"
//        playerTimerProgressConstraint.constant = 0
//    }
//    
//    func onVideoEnd(){
//        playPauseButton.setImage(UIImage(named: "player-play"), for: .normal)
//        
//        if watchAll{
//            n = true
//            videoSegmentsTableView.selectRow(at: nextIndexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
//            videoSegmentsTableView.delegate?.tableView!(videoSegmentsTableView, didSelectRowAt: nextIndexPath)
//        }
//    }
//    
//    func setupNodes(){
//        plane1 = SCNNode(geometry: SCNPlane(width: 2.1, height: 2.1))
//        plane1.pivot = SCNMatrix4MakeTranslation(0,-(plane1.boundingBox.max.y-plane1.boundingBox.min.y)/2 + 0.15,0)
//        plane1.geometry?.firstMaterial?.diffuse.contents = UIColor.clear
//        
//        plane2 = SCNNode(geometry: SCNPlane(width: 1.045, height: 1.598))
//        plane2.pivot = SCNMatrix4MakeTranslation(0,-(plane2.boundingBox.max.y-plane2.boundingBox.min.y)/2 + 0.22,0)
//        plane2.geometry?.firstMaterial?.diffuse.contents = UIColor.clear
//    }
//    
//    func setUpSceneView() {
//        sceneView.delegate = self
//        let configuration = ARWorldTrackingConfiguration()
//        configuration.planeDetection = .horizontal
//        
//        self.sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
//        //        self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
//        
//    }
//    
//    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
//        if showOval{
//            if let hitResult = sceneView.hitTest(center, types: .existingPlane).first{
//                let pos = SCNVector3(hitResult.worldTransform.columns.3.x,hitResult.worldTransform.columns.3.y, hitResult.worldTransform.columns.3.z)
//                let ac = SCNAction.move(to: pos, duration: 0.1)
//                node.runAction(ac)
//            }
//            
//        }
//    }
//    
//    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
//        switch camera.trackingState {
//        case .notAvailable:
//            print( "sessiontracking: TRACKING UNAVAILABLE")
//        case .normal:
//            print( "sessiontracking: TRACKING NORMAL")
//        case .limited(let reason):
//            switch reason {
//            case .excessiveMotion:
//                print( "sessiontracking: TRACKING LIMITED\nToo much camera movement")
//            case .insufficientFeatures:
//                print( "sessiontracking: TRACKING LIMITED\nNot enough surface detail")
//            case .initializing:
//                print( "sessiontracking: Initializing AR Session")
//            case .relocalizing:
//                print("sessiontracking: Relocalizing AR Session")
//            }
//        }
//    }
//    
//    func session(_ session: ARSession, didFailWithError error: Error) {
//        // Present an error message to the user
//        print("AR failed", error)
//    }
//    
//    func sessionWasInterrupted(_ session: ARSession) {
//        // Inform the user that the session has been interrupted, for example, by presenting an overlay
//        print("AR interrupted")
//    }
//    
//    func sessionInterruptionEnded(_ session: ARSession) {
//        // Reset tracking and/or remove existing anchors if consistent tracking is required
//        print("AR interupt endded")
//    }
//}
//
//extension matrix_float4x4 {
//    func position() -> SCNVector3 {
//        return SCNVector3(columns.3.x, columns.3.y, columns.3.z)
//    }
//}

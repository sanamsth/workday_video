//
//  ExecutiveTableViewController.swift
//  WorkdayHologram
//
//  Created by sanam on 10/29/20.
//  Copyright © 2020 VRLab. All rights reserved.
//

import UIKit

protocol VideoSelected {
    
    func playSelectedVideo(indexPath:IndexPath)
    func watchAllDelegate()
}

class ExecutiveTableViewController: UIViewController {
    @IBOutlet weak var videoSegmentsTableView: UITableView!
    
    @IBOutlet weak var executivePost: UILabel!
    @IBOutlet weak var executiveSubTitle: UILabel!
    @IBOutlet weak var executiveName: UILabel!
    
    var delegate:VideoSelected?
    
    var videoSegmentsList = [Segments](){
          didSet {
              videoSegmentsList = videoSegmentsList.sorted {$0.id < $1.id }
              
              self.videoSegmentsTableView.reloadData()
          }
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        videoSegmentsTableView.tableFooterView = UIView()
        if UIDevice.current.userInterfaceIdiom == .pad{
             executiveName.font = UIFont.preferredFont(forTextStyle: .title2)
            
        }else{
             executiveName.font = UIFont.preferredFont(forTextStyle: .headline)
        }
       
        // Do any additional setup after loading the view.
    }
    

   
    @IBAction func watchAllButton(_ sender: Any) {
        delegate?.watchAllDelegate()
    }
    
}


extension ExecutiveTableViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videoSegmentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ExecutivesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ExecutivesTableViewCell
         let segments = self.videoSegmentsList[indexPath.row]
        
        cell.SetUpCells(segments: segments)
        let selectionView = UIView()
              selectionView.backgroundColor = UIColor(red: 29/255, green: 64/255, blue: 103/255, alpha: 0.5)
              cell.selectedBackgroundView =  selectionView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate?.playSelectedVideo(indexPath: indexPath)
        
    }
    
    
    
    
}

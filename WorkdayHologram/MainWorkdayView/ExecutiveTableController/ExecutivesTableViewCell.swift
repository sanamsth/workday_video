//
//  ExecutivesTableViewCell.swift
//  WorkdayHologram
//
//  Created by sanam on 10/29/20.
//  Copyright © 2020 VRLab. All rights reserved.
//

import UIKit

class ExecutivesTableViewCell: UITableViewCell {

    @IBOutlet weak var videoStartingTimeLabel: UILabel!
    @IBOutlet weak var segmentsTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if UIDevice.current.userInterfaceIdiom == .pad{
             segmentsTitleLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        }else{
             segmentsTitleLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        }
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
//        if selected{
//            self.selectedBackgroundView?.backgroundColor = UIColor(red: 29/255, green: 64/255, blue: 103/255, alpha: 0.5)
//        }else{
//            self.backgroundColor = .clear
//        }

        // Configure the view for the selected state
    }
    
    func SetUpCells(segments:Segments){
        
        segmentsTitleLabel.text = segments.title
        videoStartingTimeLabel.text = segments.startingTime
        
    }

}

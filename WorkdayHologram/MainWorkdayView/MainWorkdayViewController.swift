//
//  MainWorkdayViewController.swift
//  WorkdayHologram
//
//  Created by sanam on 10/21/20.
//  Copyright © 2020 VRLab. All rights reserved.
//

import UIKit

class MainWorkdayViewController: UIViewController,VideoSelected,VideoStoppedDelegate {
    
    
    let images = [
        UIImage(named: "bg1")!,
          UIImage(named: "bg2")!,
            UIImage(named: "bg3")!,
              UIImage(named: "bg4")!,
        UIImage(named: "bg5")!
    ]
    
    @IBOutlet weak var footerHeight: NSLayoutConstraint!
    var collectionviewHeightConstraint:NSLayoutConstraint?
    var imgtransform = 0
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var backgroundImgView: JBKenBurnsView!
    @IBOutlet weak var executivesListView: UIView!
    @IBOutlet weak var domainLabel: UILabel!
    @IBOutlet weak var footerView: UIView!
    var executiveList: [Executives]!
    var currentExecutive: Executives?
    var db = Database()
    var numberofItemsIpad = 2.7
    var nextIndexPath: IndexPath!
    var watchAll = false
    var n = false
    var widthsize = 0.5
    
    var leadanchorconstorigin:NSLayoutConstraint?
    var leadanchorconstleft:NSLayoutConstraint?
    @IBOutlet weak var collectionViewExecutives: UICollectionView!
    
    @IBOutlet weak var videoPlayer: CustomControls!
    lazy var executiveTabeleViewController: ExecutiveTableViewController = {
        return children.lazy.compactMap({ $0 as? ExecutiveTableViewController }).first!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        addVideoPlayer()
        
        self.executiveList = self.db.getExecutives().filter(){$0.id != 10}
        homeButton.imageView?.contentMode = .scaleAspectFit
        if UIDevice.current.userInterfaceIdiom == .pad{
            widthsize = 0.5
        }else{
            widthsize = 0.62
        }
        
        addExecutivesConstraint()
        addExecutiveListCOnstraint()
        
        executiveTabeleViewController.delegate = self
        addViewTapGesture()
        videoPlayer.delegate = self
        
        videoPlayer.isHidden = true
        addOrientationNotification()
       
        
    }
    
    func addOrientationNotification(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChanged), name:UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        DispatchQueue.main.async { [self] in
            self.backgroundImgView.animateWithImages(self.images, imageAnimationDuration: 35, initialDelay: 0, shouldLoop: true, randomFirstImage: false)
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
      
    }
    
    
    @objc func orientationChanged() {
        if UIDevice.current.orientation.isLandscape {
        self.addleftrightanim(ishidden: blurView.isHidden, duration: 0.0)
        }
    }
    
   
  
 override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        backgroundImgView.layoutIfNeeded()
     
        
    }
    func addViewTapGesture(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        tap.delegate = self
        self.videoPlayer.addGestureRecognizer(tap)
        
        let blurViewTap = UITapGestureRecognizer(target: self, action: #selector(blurViewTapped))
        
        self.blurView.addGestureRecognizer(blurViewTap)
    }
    

    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    @objc func blurViewTapped(gesture:UITapGestureRecognizer){
       
        if blurView.isHidden{
           
            blurView.isHidden = false
            addleftrightanim(ishidden: blurView.isHidden, duration: 0.3)
        }else{
            
            blurView.isHidden = true
            addleftrightanim(ishidden: blurView.isHidden, duration: 0.3)
        }
        
        
    }
    @objc func viewTapped(gesture:UITapGestureRecognizer){
        if blurView.isHidden{
           
            blurView.isHidden = false
            addleftrightanim(ishidden: blurView.isHidden, duration: 0.3)
        }else{
            
            blurView.isHidden = true
            addleftrightanim(ishidden: blurView.isHidden, duration: 0.3)
        }
       
        print("view tapped")
    }
    

    func addleftrightanim(ishidden:Bool,duration:Double){
        
        
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseOut) { [self] in
            
            NSLayoutConstraint.deactivate([self.leadanchorconstorigin!])
            if ishidden{
                self.leadanchorconstorigin?.constant = -executivesListView.bounds.width
            }else{
                self.leadanchorconstorigin?.constant = 0
               
            }
            NSLayoutConstraint.activate([self.leadanchorconstorigin!])
            self.view.layoutIfNeeded()
          
           
        } completion: { (col) in
//            self.executivesListView.isHidden = true
        }
    }
    
    func addExecutiveListCOnstraint(){
        
        self.leadanchorconstorigin = executivesListView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant: 0)
        
        executivesListView.translatesAutoresizingMaskIntoConstraints = false
        
       
        NSLayoutConstraint.activate([executivesListView.topAnchor.constraint(equalTo: self.view.topAnchor),executivesListView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),self.leadanchorconstorigin!,executivesListView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: CGFloat(widthsize))])
        self.leadanchorconstleft?.isActive = false
        
    }
    
    
    func playSelectedVideo(indexPath:IndexPath) {
        
        videoPlayer.isHidden = false
        self.videoPlayer.videoPlayer.removeVideoObserver()
        backgroundImgView.isHidden = true
        self.videoPlayer.isUserInteractionEnabled = true
      
        self.blurView.isUserInteractionEnabled = true
        blurView.isHidden = true
//        executivesListView.isHidden = true
        
        

        if !n{
            watchAll = false
        }
        
        
        let segments = self.executiveTabeleViewController.videoSegmentsList[indexPath.row]
        if indexPath.row + 1 ==  self.executiveTabeleViewController.videoSegmentsList.count{
            self.nextIndexPath = IndexPath(row: 0, section: 0)
            
            self.watchAll = false
        }else{
            self.nextIndexPath = IndexPath(row: indexPath.row + 1, section: 0)
        }
        
        print("segment video ",segments.video)
        
        self.videoPlayer.videoPlayer.videolist(filename: segments.video ?? "intro.mp4")
        n = false
       
        self.addleftrightanim(ishidden: true, duration: 0.3)
        self.videoPlayer.showControls()
        
    }
    
    func VideoDidStopped() {
        
        if watchAll{
            self.videoPlayer.videoPlayer.removeVideoObserver()
            n = true
            self.executiveTabeleViewController.videoSegmentsTableView.selectRow(at: nextIndexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
            self.executiveTabeleViewController.videoSegmentsTableView.delegate?.tableView!(self.executiveTabeleViewController.videoSegmentsTableView, didSelectRowAt: nextIndexPath)
        }
    }
    
    @IBAction func HomeClicked(_ sender: Any) {
        
        self.addleftrightanim(ishidden: false, duration: 0.0)
        videoPlayer.isHidden = true
        videoPlayer.stopVideo()
        backgroundImgView.isHidden = false
        collectionViewExecutives.isHidden = false
        blurView.isHidden = false
        homeButton.isHidden = true
        executivesListView.isHidden = true
        footerView.isHidden = false
        blurView.isUserInteractionEnabled = false
        videoPlayer.isUserInteractionEnabled = false
        
    }
    
    func watchAllDelegate() {
        
        if self.executiveTabeleViewController.videoSegmentsList.count > 0{
            watchAll = true
            n = true
            nextIndexPath = IndexPath(row: 0, section: 0)
            self.executiveTabeleViewController.videoSegmentsTableView.selectRow(at: nextIndexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
            self.executiveTabeleViewController.videoSegmentsTableView.delegate?.tableView!(self.executiveTabeleViewController.videoSegmentsTableView, didSelectRowAt: nextIndexPath)
        }
    }
    
    func PreNextClicked(isNext:Bool) {
        
        if self.executiveTabeleViewController.videoSegmentsList.count > 0{
            var nextIndexPath:IndexPath!
            let selectedrow =  self.executiveTabeleViewController.videoSegmentsTableView.indexPathForSelectedRow?.row
            
            print("selected row ",selectedrow!)
            if isNext && (selectedrow!+1) < self.executiveTabeleViewController.videoSegmentsList.count{
                 nextIndexPath = IndexPath(row: selectedrow!+1, section: 0)
            }else if !isNext && selectedrow != 0{
                
                 nextIndexPath = IndexPath(row:abs(selectedrow!-1), section: 0)
            }else{
                
                return
            }
           
            
           
            self.executiveTabeleViewController.videoSegmentsTableView.selectRow(at: nextIndexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
            self.executiveTabeleViewController.videoSegmentsTableView.delegate?.tableView!(self.executiveTabeleViewController.videoSegmentsTableView, didSelectRowAt: nextIndexPath)
        }
    }
    
    @IBAction func SignOut(_ sender: Any) {
        
        let defaults = UserDefaults.standard
            defaults.set(false, forKey: defaultsKeys.loggedIn)
               
               let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navController")
               UIApplication.shared.delegate?.window??.rootViewController = vc
    }
}



extension MainWorkdayViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        let isControllTapped = (touch.view is UIStackView || touch.view is UISlider)
        return !isControllTapped
    }
}

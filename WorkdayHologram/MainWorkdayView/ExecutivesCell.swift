//
//  ExecutivesCell.swift
//  WorkdayHologram
//
//  Created by sanam on 10/29/20.
//  Copyright © 2020 VRLab. All rights reserved.
//

import UIKit

class ExecutivesCell: UICollectionViewCell {
    
    @IBOutlet weak var executiveImgeView: UIImageView!
    @IBOutlet weak var executiveTitle: UILabel!
    @IBOutlet weak var executiveJobdesc: UILabel!
    @IBOutlet weak var executiveImgContainer: UIView!
    
    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var executiveContainer: UIStackView!
    var executive: Executives!
    
    @IBOutlet weak var imgTopConst: NSLayoutConstraint!
    @IBOutlet weak var imageBottomCOnstarint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()

        if UIDevice.current.userInterfaceIdiom == .pad{
            executiveTitle.font = UIFont().setfont(font: .title)
            executiveJobdesc.font = UIFont.preferredFont(forTextStyle: .footnote)
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(deviceRotated), name: UIDevice.orientationDidChangeNotification, object: nil)
      
    }
      @objc func deviceRotated(){
        
            if UIDevice.current.orientation.isLandscape {
//                executiveImgContainer.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
            } else {
//                  executiveImgContainer.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
    
      func setUpCell(executive: Executives) {
            self.executive = executive
            self.executiveImgeView.image = UIImage(data: (executive.image ?? Data()))
            self.executiveTitle.text = executive.name
            
            self.executiveJobdesc.text = executive.title
        }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
      
    }
    
    func addCellImageConstraint(){
        executiveImgeView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([executiveImgeView.topAnchor.constraint(equalTo: self.contentView.topAnchor),executiveImgeView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),executiveImgeView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),executiveImgeView.bottomAnchor.constraint(equalToSystemSpacingBelow: self.contentView.bottomAnchor, multiplier: 0.7)])
        
        
    }
    
    func addContainerConstraint(){
        
        self.executiveContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([self.executiveContainer.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),self.executiveContainer.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),self.executiveContainer.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),self.executiveContainer.topAnchor.constraint(equalTo: executiveImgeView.bottomAnchor)])
    }
}

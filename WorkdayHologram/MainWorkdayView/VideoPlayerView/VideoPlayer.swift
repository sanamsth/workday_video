//
//  VideoPlayer.swift
//  WorkdayHologram
//
//  Created by sanam on 10/21/20.
//  Copyright © 2020 VRLab. All rights reserved.
//

import UIKit
import AVFoundation
protocol VideoTimer:class {
   
    func VideoPlaying()
    func VideoEndPlaying()
}
class VideoPlayer:UIView{
    var observer:Any?
    var player:AVPlayer?
    var isPlaying = false
    weak var delegate:VideoTimer?
    var videoLengthRight:String = "00:00"
    var videoLengthLeft:String = "00:00"
    
    lazy var activityIndicator:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .white)
        //        indicator.translatesAutoresizingMaskIntoConstraints = false
        //        indicator.center = controlsContainer.center
        indicator.startAnimating()
        return indicator
        
    }()
    
    let controlsContainer:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        
        return view
    }()
    
    var playerLayer:AVPlayerLayer?
    var isfullscreen = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        PlayerSetUp()
    }
    
    
    func videolist(filename:String){
        
        var videoURL: URL!
        
        if filename == "intro.mp4" {
            videoURL = Bundle.main.url(forResource: "blankvid", withExtension: "mp4")!
        } else {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            videoURL = documentsURL.appendingPathComponent(filename)
        }

//        videoURL = Bundle.main.url(forResource: "Carolyn_Horne_Intro", withExtension: "mp4")!
        
        player?.replaceCurrentItem(with: AVPlayerItem(url:videoURL))
        player?.play()
        
        addVideoObserver()
    }
    
    func PlayerSetUp(){
        //                let urlstring = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
        let urlstring = Bundle.main.path(forResource: "blankvid", ofType: "mp4")
        
        let playeritem = AVPlayerItem(url: URL(fileURLWithPath: urlstring!))
        
        player = AVPlayer(playerItem: playeritem)
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.videoGravity = .resizeAspectFill
        playerLayer?.masksToBounds = true
        self.layer.addSublayer(playerLayer!)
        
        //        player?.play()
        
      
                
        
    }
    
    func addVideoObserver(){
        
        self.observer = player?.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
         NotificationCenter.default.addObserver(self,selector: #selector(playerItemDidReachEnd),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: nil)
    }
    
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        
        print("pllayer end")

         self.isPlaying = false
        delegate?.VideoEndPlaying()
    }
    
    func removeVideoObserver(){
        
        if self.observer != nil{

            player?.removeObserver(self, forKeyPath: "currentItem.loadedTimeRanges")
             NotificationCenter.default.removeObserver(self)
            self.observer = nil
        }
    }
    
    func handlePlayPause(){
        
        if isPlaying{
            
            player?.pause()
        }else{
            
            player?.play()
        }
        isPlaying.toggle()
    }
    
    func restartPlay(){
        
        player?.seek(to: .zero)
       
    }
    override func layoutSubviews() {
        
        super.layoutSubviews()
        if self.isfullscreen{
            playerLayer?.frame = UIScreen.main.bounds
        }else{
            playerLayer!.frame = self.bounds
        }
        activityIndicator.frame = CGRect(x:UIScreen.main.bounds.width/2 , y: UIScreen.main.bounds.height/2, width: 0, height: 0)
        
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "currentItem.loadedTimeRanges"{
            activityIndicator.stopAnimating()
            controlsContainer.backgroundColor = .clear
            print("playing")
            if !isPlaying{
                  delegate?.VideoPlaying()
            }
            isPlaying = true
         
            
//            if let duration = player?.currentItem?.duration{
//
//                let seconds = CMTimeGetSeconds(duration)
//                let sectime =  String(format: "%02d",Int(seconds) % 60)
//                let min = String(format: "%02d",Int(seconds) / 60)
//                let righttime = "\(min):\(sectime)"
//                self.delegate?.getRightTimer(time: righttime)
//            }
        }
        
    }
    
}

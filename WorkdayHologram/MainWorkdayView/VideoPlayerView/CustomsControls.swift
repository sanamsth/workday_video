//
//  CustomsControls.swift
//  WorkdayHologram
//
//  Created by sanam on 10/23/20.
//  Copyright © 2020 VRLab. All rights reserved.
//

import AVFoundation
import UIKit

protocol VideoStoppedDelegate {
    func VideoDidStopped()
    func PreNextClicked(isNext:Bool)
}

class CustomControls:UIView,VideoTimer{
    

    @IBOutlet weak var stackControls: UIStackView!
    @IBOutlet weak var playpauseStack: UIStackView!
    @IBOutlet weak var sliderstack: UIStackView!
    
    @IBOutlet weak var videoPlayer: VideoPlayer!
    @IBOutlet weak var videoTimeLeft: UILabel!
    @IBOutlet weak var playPause: UIButton!
    @IBOutlet var controlsContainer: UIView!
    @IBOutlet weak var videTimeRight: UILabel!
    var isVideoStopped = false
    var isControlsHidden = false
    var delegate:VideoStoppedDelegate?
    
    @IBOutlet weak var timeSlider: UISlider!{
        didSet{
            self.timeSlider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
            self.timeSlider.addTarget(self, action: #selector(startEditingSlider), for: .touchDown)
            self.timeSlider.addTarget(self, action: #selector(stopEditingSlider), for: [.touchUpInside, .touchUpOutside])
            self.timeSlider.value = 0.0
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "CustomsControls", bundle: bundle).instantiate(withOwner: self, options: nil)
        controlsContainer.frame = self.bounds
        controlsContainer.layoutSubviews()
        addSubview(controlsContainer)
        addTapGesture()
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        videoPlayer.delegate = self
    }
    
    func addTapGesture(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(controlsTapped))
        tap.delegate = self
        stackControls.addGestureRecognizer(tap)
        
    }
    
    @objc func controlsTapped(gesture:UITapGestureRecognizer){
        print("controls tap")
        
        if isControlsHidden{
            showControls()
        }else{
             HideCOntrols()
        }
       
        
    }
    
    func showControls(){
        
        playpauseStack.isHidden = false
        sliderstack.isHidden = false
        isControlsHidden = false
    }
    
    func HideCOntrols(){
        
        playpauseStack.isHidden = true
                   sliderstack.isHidden = true
                   isControlsHidden = true
    }
    
    @IBAction func PlayPause(_ sender: UIButton) {
        
        handlePlayPause()
        
    }
    
    @IBAction func NextButton(_ sender: Any) {
        
        delegate?.PreNextClicked(isNext: true)
        
    }
    
    @IBAction func PreviousButton(_ sender: Any) {
        delegate?.PreNextClicked(isNext: false)
        
    }
    func handlePlayPause(){
        
        if isVideoStopped{
            videoPlayer.restartPlay()
            isVideoStopped = false
        }
        
        if videoPlayer.isPlaying{
            videoPlayer.player?.pause()
            playPause.setImage(UIImage(named: "player-play"), for: .normal)
        }else{
            
            videoPlayer.player?.play()
            
            playPause.setImage(UIImage(named: "player-pause"), for: .normal)
        }
        
        
        
        videoPlayer.isPlaying.toggle()
        
    }
    
    func stopVideo(){
        
        videoPlayer.isPlaying = false
        videoPlayer.player?.pause()
       
      
    }
 
    
    func VideoEndPlaying() {
        isVideoStopped = true
         playPause.setImage(UIImage(named: "replay-arrow"), for: .normal)
        delegate?.VideoDidStopped()
        
        
        
        
    }
    
    func VideoPlaying() {
        updateTime()
        handlePlayPause()
    }
    
    
    @objc func sliderValueChanged(){
        
       
            if let duration = videoPlayer.player?.currentItem?.duration {
             
            let totalSecond = CMTimeGetSeconds(duration)
                
              guard !(totalSecond.isNaN || totalSecond.isInfinite) else {
                                                    return
                                                }
            let currentTime = Int(Double(self.timeSlider.value) * totalSecond)
            
            self.videoTimeLeft.text = self.ytk_secondsToCounter(currentTime)
            
                let timeLeft = Int(totalSecond) - currentTime
                self.videTimeRight.text = "-\(self.ytk_secondsToCounter(timeLeft))"
            
        
           
        }
       }
    
    @objc func stopEditingSlider(){
       
      if let duration = videoPlayer.player?.currentItem?.duration {
        if !duration.isIndefinite{
                  let totalSecond = CMTimeGetSeconds(duration)
    
            
        
                  let value = Float64(timeSlider.value) * totalSecond
                  
                  let seekTime = CMTime(value: Int64(value), timescale: 1)
                  videoPlayer.player?.seek(to: seekTime, completionHandler: { (completedSeek) in
                      // later
                    self.videoPlayer.player?.play()
                  })
            
        }
              }
        
    }
    
    
      @objc func startEditingSlider(){
          
        self.stopVideo()
        
      }
    
    @objc func updateTime(){
        let currentTime = CMTimeGetSeconds((self.videoPlayer.player?.currentTime())!)
         if let duration = videoPlayer.player?.currentItem?.duration {
            let duration1 = CMTimeGetSeconds(duration)
                     
                     let progress = currentTime / duration1
                        self.timeSlider.value = Float(progress)
                     self.timeSlider.sendActions(for: .valueChanged)
                     
                     self.perform(#selector(updateTime), with: nil, afterDelay: 1.0)
        }
         
      }
    
    
    func ytk_secondsToCounter(_ seconds : Int) -> String {
        
        let time = self.secondsToHoursMinutesSeconds(seconds)
        
        let minutesSeconds = "\(self.paddedNumber(time.minutes)):\(self.paddedNumber(time.seconds))"
        
        if(time.hours == 0){
            return minutesSeconds
        }else{
            return "\(self.paddedNumber(time.hours)):\(minutesSeconds)"
        }
        
    }
    
    func secondsToHoursMinutesSeconds (_ seconds : Int) -> (hours : Int, minutes : Int, seconds : Int) {
          return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
      }
      
      func paddedNumber(_ number : Int) -> String{
          if(number < 0){
              return "00"
          }else if(number < 10){
              return "0\(number)"
          }else{
              return String(number)
          }
      }
}


extension CustomControls: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        let isControllTapped = (touch.view is UISlider)
        return !isControllTapped
    }
}

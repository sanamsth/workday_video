//
//  MainWorkday+ExecutivesList.swift
//  WorkdayHologram
//
//  Created by sanam on 10/29/20.
//  Copyright © 2020 VRLab. All rights reserved.
//

import Foundation
import UIKit


extension MainWorkdayViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.executiveList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ExecutivesCell
        let executive = self.executiveList[indexPath.row]
        
        cell.setUpCell(executive: executive)
        
        cell.imgTopConst.constant = CGFloat(self.imgtransform)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.homeButton.isHidden = false
        self.homeButton.isUserInteractionEnabled = true
        self.collectionViewExecutives.isHidden = true
        self.footerView.isHidden = true
        self.executivesListView.isHidden = false
        let executive = self.executiveList[indexPath.row]
        
        
        self.currentExecutive = executive
        self.executiveTabeleViewController.videoSegmentsList = executive.segments?.allObjects as? [Segments] ?? []
        self.executiveTabeleViewController.executiveName.text = executive.name
        self.executiveTabeleViewController.executiveSubTitle.text = executive.title
        self.executiveTabeleViewController.executivePost.text = "\"\(executive.story ?? " `")\""
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cells = self.collectionViewExecutives.visibleCells as! [ExecutivesCell]
        var c = 0
        var g = 0
        var p = 0
        for cell in cells{
            switch cell.executive.designation{
            case "Corporate Functions":
                c += 1
                break
                
            case "Go-to-Market":
                g += 1
                break
                
            case "Products & Technology":
                p += 1
                break
                
            default:
                break
            }
        }
        if c >= 3{
            UIView.animate(withDuration: 0.2, animations: {
                self.footerView.backgroundColor = UIColor(red: 15/255, green: 116/255, blue: 188/255, alpha: 1)
                
            }){ (bool) in
                self.domainLabel.text = "Corporate Functions"
            }
            
            
        }else if g >= 3{
            UIView.animate(withDuration: 0.2, animations: {
                self.footerView.backgroundColor = UIColor(red: 64/255, green: 180/255, blue: 229/255, alpha: 1)
                
            }){ (bool) in
                self.domainLabel.text = "Go-to-Market"
            }
        }else if p >= 3{
            UIView.animate(withDuration: 0.2, animations: {
                self.footerView.backgroundColor = UIColor(red: 0/255, green: 109/255, blue: 182/255, alpha: 1)
                
            }){ (bool) in
                self.domainLabel.text = "Products & Technology"
            }
        }
        print("vvvvv: ", c,g,p)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            //            print("pad")
            return CGSize(width: UIScreen.main.bounds.width/CGFloat(numberofItemsIpad), height: collectionViewExecutives.bounds.height)
        case .phone:
            print("iphone")
            return CGSize(width: UIScreen.main.bounds.width/CGFloat(numberofItemsIpad), height: collectionViewExecutives.bounds.height)
        default:
            print("other")
        }
        
        return CGSize(width: collectionView.frame.width/3, height: collectionViewExecutives.frame.height-10)
        
    }
    
    
    func addExecutivesConstraint(){
        collectionViewExecutives.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([collectionViewExecutives.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),collectionViewExecutives.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),collectionViewExecutives.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),collectionViewExecutives.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),collectionViewExecutives.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.55)])
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if UIApplication.shared.statusBarOrientation.isLandscape {
            // activate landscape changes
            
            self.numberofItemsIpad = 2.7
            self.imgtransform = 0
            if UIDevice.current.userInterfaceIdiom == .phone{
                
                self.footerHeight.constant = 75
            }
        } else {
            
            self.numberofItemsIpad = 4.4
            self.imgtransform = -60
            if UIDevice.current.userInterfaceIdiom == .phone{
                
                self.footerHeight.constant = 60
            }
            
        }
        guard let flowLayout = collectionViewExecutives.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
        collectionViewExecutives.reloadData()
    }
    
}

//
//  FontSelection.swift
//  TrigonometryFormulas
//
//  Created by sanam on 11/2/19.
//  Copyright © 2019 KTM Studio. All rights reserved.
//

import Foundation
import UIKit

enum fontselection{
    
    case title
    case subtitle
    
   
}

extension UIFont{
    
    func setfont(font:fontselection) -> UIFont{
        
        if UIDevice().userInterfaceIdiom == .pad{
            
            switch font{
                
            case .title:
                return UIFont.preferredFont(forTextStyle: .title2).bold()
            case .subtitle:
                
                    return UIFont.preferredFont(forTextStyle: .headline).bold()
            }
        }else{
            
            switch font{
                
            case .title:
                return UIFont.preferredFont(forTextStyle: .headline).bold()
            case .subtitle:
                
                return UIFont.preferredFont(forTextStyle: .footnote).bold()
            }
        }
    }
    
}



extension UIFont {
    func withTraits(traits:UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
    
    func italic() -> UIFont {
        return withTraits(traits: .traitItalic)
    }
}

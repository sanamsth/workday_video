//
//  SignInViewController.swift
//  WorkdayHologram
//
//  Created by mac on 12/10/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import UIKit
import Alamofire

class SignInViewController: UIViewController {

    @IBOutlet weak var emailTextField: PaddableTextField!
    @IBOutlet weak var passwordTextField: PaddableTextField!
    
    @IBOutlet weak var resendVerification: UIButton!
    var alertView: AlertRibbonView!

      let reachability =  try! Reachability()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.delegate = self
          UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        alertView = AlertRibbonView(frame: CGRect(x: 0, y: -70, width: self.view.frame.width, height: 70))
        self.view.addSubview(alertView)
        setTransparentNavBar()
        checkLogin()
        reachability.whenReachable = { _ in
                DispatchQueue.main.async {
                  
                    print("reachable")
                }
            }

            reachability.whenUnreachable = { _ in
                DispatchQueue.main.async {
                     print("unreachable")
                   self.presentInternetAlert(withTitle: "Connection Lost", message: "\nYour device has lost connection to server.Check internet connection.")
                    self.hideProgressHud()
                }
            }

           do{
                   try reachability.startNotifier()
               } catch {
                   print("Could not strat notifier")
               }
        
    }
    
    
  public override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    func setTransparentNavBar(){
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.barStyle = .black
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        resendVerification.isHidden = true
    }
   
    
    func checkLogin(){
        let defaults = UserDefaults.standard
        
        let login = defaults.bool(forKey: defaultsKeys.loggedIn)
        if login{
            startApp()
            
        }
    }
    
    func startApp(){
        
        self.hideProgressHud()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController")
        UIApplication.shared.delegate?.window??.rootViewController = vc
    }
    @IBAction func resendVerification(_ sender: Any) {
        let email = emailTextField.text ?? ""
        
        if email != "" {
         
            self.resendVerification(email: email)
        }
    
    }
    
    func resendVerification(email: String){
        
        if !email.validate(validDomains: validDomains){
            self.alertView.showAlert(alertCase: alertCases.invalidEmail)
        }
        let url = "\(URLs.baseUrl)\(URLs.resendVerification)"  // This will be your link
        let parameters: Parameters = ["email": email]    //This will be your parameter
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
         
            if let json = response.result.value as? [String: Any]{
                
                self.alertView.showAlert(alertCase: alertCases.forgotPassword)
                if let code = json["code"] as? String{
                    
                    switch code{
                    case "0001":
                        self.alertView.showAlert(alertCase: alertCases.signup)
                        break
                    default:
                        self.alertView.showAlert(alertCase: alertCases.failure)
                        break
                    }
                }
            }
        }
    }
    
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func signInBtnTapped(_ sender: Any) {
        
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        if !email.validate(validDomains: validDomains) && email != "" && password != ""{
            self.alertView.showAlert(alertCase: alertCases.invalidEmail)
            
            return
            
        }
        
        if email != "" && password != ""{
            self.showProgressHud()
            let url = "\(URLs.baseUrl)\(URLs.login)"  // This will be your link
            let parameters: Parameters = ["email": email, "password": password ]    //This will be your parameter
            
            Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
                
                switch response.result {
                    case .success(_):
                        // internet works.
                                  if let json = response.result.value as? [String: Any]{
                                      if let code = json["code"] as? String{
                                          
                                          print("mycode ", code)
                                          switch code{
                                          case "0025":
                                            self.alertView.showAlert(alertCase: alertCases.noAccess)
                                            self.hideProgressHud()
                                              break
                                          case "0024":
                                              self.alertView.showAlert(alertCase: alertCases.wrongCredentials)
                                            self.hideProgressHud()
                                              break
                                          case "0026":
                                            self.resendVerification.isHidden = false
                                            self.alertView.showAlert(alertCase: alertCases.unverified)
                                            self.hideProgressHud()
                                          case "-1009":
                                              
                                              
                                              break
                                          default:
                                              self.login()
                                              break
                                          }
                                      }
                                  }
                    case .failure(let error):

                        if error._code == NSURLErrorNotConnectedToInternet {
                            DispatchQueue.main.async {
                                self.presentInternetAlert(withTitle: "Connection Lost", message: "\nYour device has lost connection to server.Check internet connection.")
                                self.hideProgressHud()
                            }
                     
                           
                        } else {
                            // other failures
                        }
                    }
              
            }
        }else{
            self.alertView.showAlert(alertCase: alertCases.noEmailAndPassword)
            self.hideProgressHud()
        }
    }
    
    func login(){
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: defaultsKeys.loggedIn)
        startApp()
    }
    
    @IBAction func SignUpBtnTappeed(_ sender: Any) {
        let registerCase = registerCases.register
        performSegue(withIdentifier: "showSignUpViewController", sender: registerCase)
    }
    
    @IBAction func forgotPasswordBtnTapped(_ sender: Any) {
        let registerCase = registerCases.forgotPassword
        
        performSegue(withIdentifier: "showSignUpViewController", sender: registerCase)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let registerCase = sender as? registerCases{
            let vc = segue.destination as! SignUpViewController
            vc.registerCase = registerCase
        }
    }
}


import Foundation
import UIKit

@IBDesignable
class PaddableTextField: UITextField {
    
    var padding = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    
    @IBInspectable var left: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var right: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var top: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var bottom: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    func adjustPadding() {
        padding = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
}

extension SignInViewController:UINavigationControllerDelegate {

open override var shouldAutorotate: Bool {
    return true
}

    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
}

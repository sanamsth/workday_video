//
//  ExecutivesCollectionViewCell.swift
//  WorkdayHologram
//
//  Created by VRLab on 11/5/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import UIKit

class ExecutivesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var executiveImageView: UIImageView!
    @IBOutlet weak var executiveNameLabel: UILabel!
    @IBOutlet weak var executivePositionLabel: UILabel!
    @IBOutlet weak var flagView: UIView!
    @IBOutlet weak var flagchildView: UIView!
    @IBOutlet weak var domainLabel: UILabel!
    
    var executive: Executives!
    
    func setUpCell(executive: Executives) {
        self.executive = executive
        self.executiveImageView.image = UIImage(data: (executive.image ?? Data()))
        self.executiveNameLabel.text = executive.name
        self.executivePositionLabel.text = executive.title
        self.domainLabel.text = executive.designation
//        showFlagView()
    }
    
    func showFlagView(){
//        if executive.name == "Angela Stark" || executive.name == "Chris Curtis" || executive.name == "Andrea  Ruiz" {
            flagView.isHidden = false
            switch executive.designation{
            case "Corporate Functions":
                flagView.backgroundColor = UIColor(red: 15/255, green: 116/255, blue: 188/255, alpha: 1)
                flagchildView.backgroundColor = UIColor(red: 15/255, green: 116/255, blue: 188/255, alpha: 1)
                break
                
            case "Go-to-Market":
                flagView.backgroundColor = UIColor(red: 64/255, green: 180/255, blue: 229/255, alpha: 1)
                flagchildView.backgroundColor = UIColor(red: 64/255, green: 180/255, blue: 229/255, alpha: 1)
                break
                
            case "Products & Technology":
                flagView.backgroundColor = UIColor(red: 0/255, green: 109/255, blue: 182/255, alpha: 1)
                flagchildView.backgroundColor = UIColor(red: 0/255, green: 109/255, blue: 182/255, alpha: 1)
                break
                
            default:
                break
            }
//        }
//        else{
//            flagView.isHidden = true
//        }
    }
    
    func hideFlagView(){
        flagView.isHidden = true
    }
    
}

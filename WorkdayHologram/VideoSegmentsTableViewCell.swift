//
//  VideoSegmentsTableViewCell.swift
//  WorkdayHologram
//
//  Created by VRLab on 11/6/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import UIKit

class VideoSegmentsTableViewCell: UITableViewCell {

    @IBOutlet weak var segmentsTitleLabel: UILabel!
    @IBOutlet weak var videoStartingTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

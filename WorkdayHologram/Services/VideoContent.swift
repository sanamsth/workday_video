//
//  VideoContent.swift
//  SceneKitTransparentVideo
//
//  Created by Turkowski on 21/09/2017.
//  Copyright © 2017 Turkowski. All rights reserved.
//

import SpriteKit
import AVFoundation

class VideoContent: SKScene {
    
    // MARK: - Private properties
    
    var player: AVPlayer
    var isPlaying = false
    var timeObserver: Any?
    
    // MARK: - Initialization
    
    init(filename: String, size: CGSize, sender: MainViewController) {
        var videoURL: URL!
        
        if filename == "intro.mp4" {
            videoURL = Bundle.main.url(forResource: filename, withExtension: nil)!
        } else {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            videoURL = documentsURL.appendingPathComponent(filename)
        }
        
        player = AVPlayer(url: videoURL)
        
        super.init(size: size)
        
        backgroundColor = .clear
        scaleMode = .aspectFit
        
        let videoSpriteNode = SKVideoNode(avPlayer: player)
        videoSpriteNode.position = CGPoint(x: size.width/2, y: size.height/2)
        videoSpriteNode.size = size
        videoSpriteNode.yScale = -1
        videoSpriteNode.play()
        isPlaying = true
        sender.playPauseButton.setImage(UIImage(named: "player-pause"), for: .normal)
        addChild(videoSpriteNode)
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main)
            {
                [unowned self, unowned sender] _ in
//                player.seek(to: CMTime.zero)
//                player.play()
//                sender.onVideoEnd()
                self.isPlaying = false
        }
        
        timeObserver = player.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 100), queue:DispatchQueue.main) {
            [unowned self] time in
            guard let duration = self.player.currentItem?.duration else { return }
            let tot = CMTimeGetSeconds(duration)
            
            sender.totalTimeLabel.text = duration.durationText
            
            let cur = CMTimeGetSeconds(time)
            let timeString = String(format: "%02.2f", cur)
            if cur != 0{
                sender.playerTimerProgressConstraint.constant = CGFloat(302 - (cur / tot * 302))
            }
            sender.currentTimeLabel.text = time.durationText
            
            print("time is \(timeString)")
            
        }
    }
    
    func removeObserver(){
        NotificationCenter.default.removeObserver(self)
        
        if let timeObserver = timeObserver{
            self.player.removeTimeObserver(timeObserver)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension CMTime {
    var durationText:String {
        if self == CMTime.zero || self == .indefinite {
            return "00:00"
        }
        let totalSeconds = CMTimeGetSeconds(self)
        let hours:Int = Int(totalSeconds / 3600)
        let minutes:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 3600) / 60)
        let seconds:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            return String(format: "%i:%02i:%02i", hours, minutes, seconds)
        } else {
            return String(format: "%02i:%02i", minutes, seconds)
        }
    }
}


//
//  Database.swift
//  WorkdayHologram
//
//  Created by mac on 11/1/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import UIKit
import CoreData

class Database{
    
    let context = PersistenceService.context
    
    init() {
        context.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
    }
    
    func saveExecutive(id: Int, name: String, title: String, designation: String, imageName: String, story: String) -> Executives?{
          print("exit id ",id)
        if isExist(id: Int16(id), enityName: "Executives"){
              
           
            deleteAttribute(entity: "Executives", id: id)
            
        }
        let executive = Executives(context: context)
        
        executive.id = Int16(id)
        executive.name = name
        executive.title = title
        executive.imageName = imageName
        executive.story = story
        executive.designation = designation
         
        do{
            try context.save()
            print("saved")
        }catch{
            print("not saved")
        }
        
        return executive
    }
    
    func saveSegment(id: Int, title: String, video: String, startingTime: String, executive: Executives){
        
        if isExist(id: Int16(id), enityName: "Segments"){
            print("exist segment")
            return
        }
        let segment = Segments(context: context)
        
        segment.id = Int16(id)
        segment.startingTime = startingTime
        segment.title = title
        segment.video = video
        segment.executive = executive
        
        do{
            try context.save()
            print("saved")
        }catch{
            print("not saved")
        }
    }
    
    func deleteDB(entity: String){
        
        let context = PersistenceService.context
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
            print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        } catch {
            print ("There was an error")
        }
    }
    
    func deleteAttribute(entity: String,id:Int){
        
        let context = PersistenceService.context
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        
        deleteFetch.predicate = NSPredicate(format: "id=%d",id)
        
        let objects = try! context.fetch(deleteFetch)
         let resultData = objects as! [Executives]
        for obj in resultData {
            print("deleted ",obj.name)
            context.delete(obj)
        }

        do {
            try context.save() // <- remember to put this :)
        } catch {
            // Do something... fatalerror
        }
    }
    
    func addExecutiveImage(executive: Executives, image: UIImage){
        executive.image = UIImage.pngData(image)()
        do{
            try context.save()
            print("saved")
        }catch{
            print("not saved")
        }
    }
    
    func getExecutives() -> [Executives]{
        let request: NSFetchRequest<Executives> = Executives.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "designation", ascending: false)]
        
        let frc = NSFetchedResultsController<Executives>(
            fetchRequest: request,
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        do{
            try frc.performFetch()
        }catch{
            print("caught")
        }
        print("debug: activity")
        for o in frc.fetchedObjects!{
            
            print(o)
        }
        return frc.fetchedObjects!
    }
    
    func isExist(id: Int16,enityName:String) -> Bool {
        
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: enityName)
    fetchRequest.predicate = NSPredicate(format: "id=%d",id)
        fetchRequest.fetchLimit = 1

    let res = try! context.fetch(fetchRequest)
    return res.count > 0 ? true : false

}

}

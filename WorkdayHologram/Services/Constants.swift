//
//  Constants.swift
//  WorkdayHologram
//
//  Created by mac on 11/1/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import Foundation

struct URLs {
    static let baseUrl1 = "http://ec2-34-214-76-102.us-west-2.compute.amazonaws.com/workday/"
    static let baseUrl = "https://www.paracosma.com/workday/"
    static let getAuthentication = "api/get_authentication"
    static let getExecutives = "api/getExecutive"
    static let videoUrl = "public/uploads/v1/"
    static let imageUrl = "public/images/models/"
    static let getTimestamp = "api/getTimeStamps"
    static let register = "api/register"
    static let login = "api/login"
    static let forgotPassword = "api/forgot-password"
    static let resendVerification = "public/api/resend"
    
}

struct defaultsKeys {
    static let downloaded = "downloaded"
    static let timestamp = "timestamp"
    static let loggedIn = "loggedIn"
}

enum alertCases: String{
    case noAccess = "Your email doesnot have access to the app. Please contact the admin."
    case wrongCredentials = "Wrong username or password."
    case signup = "Please check your email."
    case forgotPassword = "We have emailed your password reset link!"
    case noEmailAndPassword = "Please enter email and password."
    case alreadyRegistered = "Your email is already registered. Please check your email."
    case noEmail = "Please enter email."
    case noUsername = "Please enter username."
    case noPassword = "Please enter password."
    case notRegistered = "This email is not registered.Please Sign up."
    case invalidEmail = "Invalid email address."
    case unverified = "Email is not Verified."
    case failure = "Something went wrong."
}

enum registerCases: String{
    case register = "Sign Up To Your Account"
    case forgotPassword = "Reset Your Password"
}


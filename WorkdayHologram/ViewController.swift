//
//  ViewController.swift
//  WorkdayHologram
//
//  Created by VRLab on 10/31/18.
//  Copyright © 2018 VRLab. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    private let db = Database()
    private let defaults = UserDefaults.standard
    
    private var totalVideos = 0
    private var downloadedVideos = 0
    private var totalExecutives = 0
    private var downloadedExecutives = 0
    var videos:[String]?
    var internetAvailable = true
    private let serialQueue = DispatchQueue(label: "queuename")
      let totalExecutiveSaved = UserDefaults.standard
    @IBOutlet weak var downloadLabel: UILabel!
    @IBOutlet weak var patientLabel: UILabel!
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var leaderLabel: UILabel!
    @IBOutlet weak var logoImg: UIImageView!
    var alert:UIAlertController?
    private var timestamp = ""
    let reachability =  try! Reachability()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.logoImg.transform = CGAffineTransform(scaleX: 0.01, y: 0.01);
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseOut, animations: {
            self.logoImg.transform = .identity
        }) { (completed) in
            
        }
       
        
        if defaults.object(forKey: "nExecutive") == nil{
            self.totalExecutiveSaved.set(0, forKey: "nExecutive")
            self.totalExecutiveSaved.set(0, forKey: "totalVideos")
             self.totalExecutiveSaved.set(0, forKey: "lastSegment")
           
        }
        self.videos = defaults.object(forKey: "mVideos") as? [String] ?? [String]()
        self.downloadedExecutives = self.totalExecutiveSaved.integer(forKey: "nExecutive")
        print("download exe ",downloadedExecutives)
        
        self.totalVideos = self.totalExecutiveSaved.integer(forKey: "totalVideos") - self.totalExecutiveSaved.integer(forKey: "lastSegment")
                 print("ntotal ",self.downloadedExecutives)
                  self.downloadLabel.text = "Downloading Executives"
                 versionLabel.text = "version \(Bundle.main.releaseVersionNumber ?? "")"
        UIApplication.shared.isIdleTimerDisabled = true
        
        if UIDevice.current.userInterfaceIdiom == .pad{
           
            leaderLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle).bold()
            versionLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
            
            
        }else{
             leaderLabel.font = UIFont.preferredFont(forTextStyle: .title2).bold()
        }
            
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
               if self.downloadedExecutives != 0{
                          self.downloadedExecutives -= 1
                      }
                if self.alert != nil{
                     self.alert!.dismiss(animated: true, completion: nil)
                }
               
                self.internetAvailable = true
                self.checkChanges()
                print("reachable")
            }
        }

        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                 print("unreachable")
                self.internetAvailable = false
                self.hideProgressHud()
                self.showInternetAlert()
            }
        }

       do{
               try reachability.startNotifier()
           } catch {
               print("Could not strat notifier")
           }
       
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }

    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.portrait
    }
    
    @IBAction func startBtnTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "MainWorkdaySb", bundle: nil).instantiateInitialViewController() as! MainWorkdayViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    func checkChanges(){
        if !internetAvailable {
            return
        }
        
        Alamofire.request("\(URLs.baseUrl)\(URLs.getTimestamp)").responseJSON { response in
            
            switch response.result {
                  case .success(_):
                     guard let json = response.result.value as? [[String: Any]] else{return}
                              print("JSON: \(response)") // serialized json response
                              guard let timestamp = json.first?["table_time"] as? String else{return}
                              guard let savedTimestap = self.defaults.string(forKey: defaultsKeys.timestamp) else{
                                  self.checkDownload()
                                  self.timestamp = timestamp
                                  return
                              }
                              
                              if savedTimestap == timestamp{  // no changes in server
                                  DispatchQueue.main.async {
                                      self.getStartedButton.isHidden = false
                                  }
                              }else{ //changes in server
                                  self.defaults.set(false, forKey: defaultsKeys.downloaded) // saying that new data is not downloaded
                                  self.checkDownload()
                                  self.timestamp = timestamp
                              }
                  case .failure(_):
                    
                    self.checkChanges()
                      //error tells me 403
                      //response.result.data can't be cast to NSDictionary or NSArray like
                      //the successful cases, how do I get the response body?
              }
            
         
            
           
        }
    }
    
    func connectedToInternet() -> Bool{
        if Reachability1.isConnectedToNetwork(){
            print("Internet Connection Available!")
            return true
        }else{
//            getStartedButton.isHidden = false
            print("Internet Connection not Available!")
            return false
        }
    }
    
    func checkDownload(){
        if (!defaults.bool(forKey: defaultsKeys.downloaded)){
//            db.deleteDB(entity: "Executives")
//            db.deleteDB(entity: "Segments")
            patientLabel.isHidden = false
            downloadLabel.isHidden = false
            downloadDatas(success: {
            }) {
                print("Cant download data now")
                
                // handle error
            }
        }else{
//            getStartedButton.isHidden = false
        }
    }
    
    func showInternetAlert(){
        print("alert")
       
            DispatchQueue.main.async {
                 if self.presentedViewController == nil{
                    
                    
                    if (!self.defaults.bool(forKey: defaultsKeys.downloaded)){
                        
                        self.presentInternetAlert(withTitle: "Connection Lost", message: "\nYour device has lost connection to server.Check internet connection.")
                    }else{
                        
                        self.getStartedButton.isHidden = false
                    }
                    
               
                   }
                
            }
       
    }
    
    func downloadDatas(success: @escaping () -> (), failure: @escaping () -> ()){
        self.showProgressHud()
        
        if !internetAvailable {
            return
            
        }
        
        Alamofire.request("\(URLs.baseUrl)\(URLs.getExecutives)").responseJSON { response in
           
            // queue because we dont want to download everything parallelly
            self.serialQueue.async {
                print("This is run on the background queue")
                guard let json = response.result.value as? [String: Any] else{failure();return}
                print("JSON: \(json)") // serialized json response
                
                guard let executives = json["executive_list"] as? [[String: Any]] else{failure();return}
                self.totalExecutives = executives.count
                
                
                
                for i in self.downloadedExecutives..<self.totalExecutives{
                    print("======")
                    if !self.internetAvailable{
                        self.showInternetAlert()
                               
                               return
                           }
                    let data = executives[i]
//                    print(data)
                    print("exit downloadid ",i)
                        self.downloadedExecutives += 1
                               
                    DispatchQueue.main.async {
                                  self.totalExecutiveSaved.set(self.downloadedExecutives, forKey: "nExecutive")
//                                self.totalExecutiveSaved.set(self.videos, forKey: "mVideos")
                            }
                    guard let executive = self.saveExecutive(data: data) else{return}
                    self.downloadImage(executive: executive)
             
//                    self.startVideoDownload()
                    DispatchQueue.main.async {
                        print("This is run on the main queue, after the previous code in outer block")
                        self.downloadLabel.text = "Downloading Executives \(self.downloadedExecutives)/\(self.totalExecutives)"
                       
                    
                        
                    }
                    if let segments = data["segment"] as? [[String: Any]]{
                        self.totalVideos += segments.count
                          self.totalExecutiveSaved.set(self.totalVideos, forKey: "totalVideos")
                        self.totalExecutiveSaved.set(segments.count, forKey: "lastSegment")
                      
                        print("tv dv: ",self.totalVideos,self.downloadedVideos)
                        for data in segments{
                            print("xxxxxxxxxxxxxx")
                            print(data)
                            
                            self.saveSegments(data: data, executive: executive)
                        }
                    }
                }
                self.startVideoDownload()
                success()
            }
        }
    }
    
    func startVideoDownload(){
        if self.downloadedExecutives == self.totalExecutives{
        
            let mVideos = self.videos?.orderedSet
            self.totalVideos = mVideos!.count
            print("mvideos 2 ",mVideos!.count)
                              // start downloading videos once all executive data is downloaded
            self.downloadVideo(video: mVideos![self.downloadedVideos])
                              
            print("download videos ",mVideos![self.downloadedVideos]);
                          }
    }
    
    
    func saveExecutive(data: [String: Any]) -> Executives?{
        guard let id = data["id"] as? Int else{return nil}
        guard let name = data["name"] as? String else{return nil}
        guard let title = data["title"] as? String else{return nil}
        guard let designation = data["designation"] as? String else{return nil}
        guard let imageName = data["image"] as? String else{return nil}
        guard let story = data["storyname"] as? String else{return nil}
       
        
        return db.saveExecutive(id: id, name: name, title: title, designation: designation, imageName: imageName, story: story)
    }
    
    func saveSegments(data: [String: Any], executive: Executives){
        guard let id = data["id"] as? Int else{return}
        guard let startingTime = data["starting_time"] as? String else{return}
        guard let title = data["title"] as? String else{return}
        guard let video = data["video"] as? String else{return}
        
        self.videos!.append(video)
       
//        print("mvideos ",self.videos)
        db.saveSegment(id: id, title: title, video: video, startingTime: startingTime, executive: executive)
        DispatchQueue.main.async {
//                                          self.totalExecutiveSaved.set(self.downloadedExecutives, forKey: "nExecutive")
                                        self.totalExecutiveSaved.set(self.videos, forKey: "mVideos")
                                    }
       
    }
    
    
    // here are lots of use of recursive download; lazy way to handle error
    func downloadImage(executive: Executives){
        if internetAvailable{
            let urlString = "\(URLs.baseUrl)\(URLs.imageUrl)\(executive.imageName ?? "")"
                   let url = URL(string: urlString)
                   if let data = try? Data(contentsOf: url!) {
                       if let image = UIImage(data: data) {
                           print(image)
                           self.db.addExecutiveImage(executive: executive, image: image)
                       }else{
                           print("Something went wrong!")
                           if internetAvailable{
                                 downloadImage(executive: executive)
                           }
                         
                       }
                   }
                   else{
                       print("Something went wrong!")
                       if internetAvailable{
                                            downloadImage(executive: executive)
                        }
                   }
        }
       
    }
    
    func downloadVideo(video: String){
        
        if !internetAvailable {
            return
            
        }
        self.serialQueue.async{
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let videoURL = documentsURL.appendingPathComponent(video)
            print("file: ",video)
            
            //if video exist (already downloaded, try block will execute) download next video on queue else (on catch) download the video
        do{
            let _ = try videoURL.checkResourceIsReachable()
            self.downloadedVideos += 1
            print("file found", self.totalVideos, self.downloadedVideos)
            DispatchQueue.main.async {
                self.downloadLabel.text = "Downloading Videos \(self.downloadedVideos)/\(self.totalVideos)"
                if self.totalVideos <= self.downloadedVideos{
                    self.hideProgressHud()
                    self.defaults.set(true, forKey: defaultsKeys.downloaded)
                    self.downloadLabel.isHidden = true
                    self.patientLabel.isHidden = true
                    self.getStartedButton.isHidden = false
                }else{
                    self.downloadVideo(video: self.videos![self.downloadedVideos])
                }
            }
            
        }catch{
            print("file not found",video)

            
            let url = "\(URLs.baseUrl1)\(URLs.videoUrl)\(video)"
            
            print("video url ",url)
            
            Alamofire.request(url).responseData{ (response) in

                if let data = response.result.value {
                    print ("vvvvvvv:  \(response.result)")
                    if "\(response.result)" == "SUCCESS"{

                        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                        let videoURL = documentsURL.appendingPathComponent(video)
                        do {
                            try data.write(to: videoURL)
                            print(videoURL)
                            print("success")
                            self.downloadedVideos += 1
                            print("tv dv ddd:", self.totalVideos, self.downloadedVideos)
                            DispatchQueue.main.async {
                                self.downloadLabel.text = "Downloading Videos \(self.downloadedVideos)/\(self.totalVideos)"
                                if self.totalVideos <= self.downloadedVideos{
                                    self.hideProgressHud()
                                    self.defaults.set(true, forKey: defaultsKeys.downloaded)
                                    self.defaults.set(self.timestamp, forKey: defaultsKeys.timestamp)
                                    self.downloadLabel.isHidden = true
                                    self.patientLabel.isHidden = true
                                    self.getStartedButton.isHidden = false
                                }else{
                                    self.downloadVideo(video: self.videos![self.downloadedVideos])
                                }
                            }
                        } catch {
                            print("Something went wrong!")
                            self.downloadVideo(video: video)
                        }
                    }else{
                        print("Something went wrong!")
                        self.downloadVideo(video: video)
                    }

                }
                else{
                    print("Something went wrong!")
                    self.downloadVideo(video: video)
                }
            }
            
            }
            
        }
        
    }
    
}

// to check running thread
// not used in this project
extension Thread {
    class func printCurrent() {
        print("\r⚡️: \(Thread.current)\r" + "🏭: \(OperationQueue.current?.underlyingQueue?.label ?? "None")\r")
    }
}


extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}


extension RangeReplaceableCollection where Element: Hashable {
    var orderedSet: Self {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
    mutating func removeDuplicates() {
        var set = Set<Element>()
        removeAll { !set.insert($0).inserted }
    }
}


extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}


extension UIViewController{
    
  func presentInternetAlert(withTitle title: String, message : String) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "Dismiss", style: .cancel) { action in
          print("You've pressed cancel Button")
      }
      alertController.addAction(OKAction)
      self.present(alertController, animated: true, completion: nil)
    }
}
